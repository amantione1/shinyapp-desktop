library(data.table)
library(ggplot2)

rm(list = ls())
gc()

# Identify all LFS indicators for creating possible Shiny Dashboard

# A. INDICATOR: WAGES (AVG HRLY, MEDIAN HRLY)
# 1.  YEAR, REGION, IMMIGRANT STATUS, AGE GROUP, and NAICS

# 2. YEAR, REGION, IMMIGRANT STATUS, AGE GROUP, and NOC/TEER

# B. 

#### READ IN ALL LFS PUMPF FILES AND APPEND INTO ONE DATASET
folder_path <- "C:/Users/Anthony/Desktop/LFS data"

files <- list.files(path = folder_path,
                    pattern = "\\.csv$", 
                    full.names = T
)

data.tables <- lapply(files, fread)

dt <- rbindlist(data.tables)

rm(data.tables)

#### DO SOME CLEANING TO CREATE A TABLE OF INDICATORS --------------------------
dt[, gender := fcase(SEX == 1, "Male",
                     SEX == 2, "Female"
                     )
   ]

dt[, prov := fcase(PROV == 10, "Newfoundland and Labrador",
                   PROV == 11, "Prince Edward Island",
                   PROV == 12, "Nova Scotia",
                   PROV == 13, "New Brunswick",
                   PROV == 24, "Quebec",
                   PROV == 35, "Ontario",
                   PROV == 46, "Manitoba",
                   PROV == 47, "Saskatchewan",
                   PROV == 48, "Alberta",
                   PROV == 59, "British Columbia")]

dt[, age := fcase(AGE_12 %in% c(1, 2), "15-24",
                  AGE_12 %in% c(3,4,5,6,7,8), "25-54",
                  default = "55+")]

dt[, immigrant := fcase(IMMIG %in% c(1,2), "Permanent residents",
                        IMMIG == 3, "Non-immigrants")
   ]

dt[!is.na(HRLYEARN), hrlyearn := HRLYEARN/100]

# Median wage is quite complex (resource intensive) to calculate. 
# ** IT WON'T CALCULATE -- TOO MUCN MEMORY DEMAND; NEED TO FIND ALTERNATIVE **
# dt[!is.na(hrlyearn), 
#    expanded_wage := lapply(round(FINALWT/12), function(x) rep(hrlyearn, x))]

#### CREATE THE SUMMARY DATA TABLE ---------------------------------------------
# Note this is all monthly data, so to make it annual; I will need to multiply 
# by (FINALWT/12) -- or simply take the average across 12 months. 

# Data Table
# User selects: Year, Geography, Age, Gender

# Values are shown for PRs, Non-PRs, and Total population
# Population : sum(FINALWT/12)
# Labour force : sum(FINALWT/12) where LFSSTAT != 4
# Employment : sum(FINALWT/12) where LFSSTAT %in% c(1,2)
# Unemployment : sum(FINALWT/12) where LFSSTAT == 3
# Unemployment rate : unemployment / labour force
# Participation rate : labour force / population
# Employment rate : employment / population

# Create indicators with all filters (year, immigrant, prov, age, gender)
cut1 <- dt[, .(Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, immigrant, prov, age, gender)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                 `Labour Force`, Employment, Unemployment, 
                 `Unemployment Rate` = Unemployment/`Labour Force`,
                 `Participation Rate` = `Labour Force`/Population,
                 `Employment Rate` = Employment/Population,
                 `Average Hourly Wage`)]

# no filters (year only)
cut2 <- dt[, .(immigrant = "Total population",
               prov = "Canada", 
               age = "15+",
               gender = "All genders",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
                 by = .(SURVYEAR)
                 ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                       `Labour Force`, Employment, Unemployment, 
                       `Unemployment Rate` = Unemployment/`Labour Force`,
                       `Participation Rate` = `Labour Force`/Population,
                       `Employment Rate` = Employment/Population,
                       `Average Hourly Wage`)]

# total population (prov, age, gender, prov&age, prov&gender, age&gender)
cut3 <- dt[, .(immigrant = "Total population",
               age = "15+",
               gender = "All genders",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, prov)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut4 <- dt[, .(immigrant = "Total population",
               prov = "Canada",
               gender = "All genders",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, age)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut5 <- dt[, .(immigrant = "Total population",
               prov = "Canada",
               age = "15+",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, gender)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut6 <- dt[, .(immigrant = "Total population",
               gender = "All genders",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, prov, age)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut7 <- dt[, .(immigrant = "Total population",
               age = "15+",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, prov, gender)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut8 <- dt[, .(immigrant = "Total population",
               prov = "Canada",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, age, gender)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut9 <- dt[, .(immigrant = "Total population",
               Population = sum(FINALWT/12),
               `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
               Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
               Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
               `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
           by = .(SURVYEAR, prov, age, gender)
           ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

# immigrants (prov, age, gender, prov&age, prov&gender, age&gender)
cut10 <- dt[, .(prov = "Canada",
                age = "15+",
                gender = "All genders",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]
  
cut11 <- dt[, .(age = "15+",
                gender = "All genders",
                Population = sum(FINALWT/12),
                   `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                   Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                   Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
               by = .(SURVYEAR, immigrant, prov)
               ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                     `Labour Force`, Employment, Unemployment, 
                     `Unemployment Rate` = Unemployment/`Labour Force`,
                     `Participation Rate` = `Labour Force`/Population,
                     `Employment Rate` = Employment/Population,
                     `Average Hourly Wage`)]

cut12 <- dt[, .(prov = "Canada",
                gender = "All genders",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant, age)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
      `Labour Force`, Employment, Unemployment, 
      `Unemployment Rate` = Unemployment/`Labour Force`,
      `Participation Rate` = `Labour Force`/Population,
      `Employment Rate` = Employment/Population,
      `Average Hourly Wage`)]

cut13 <- dt[, .(prov = "Canada", 
                age = "15+",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant, gender)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                  `Labour Force`, Employment, Unemployment, 
                 `Unemployment Rate` = Unemployment/`Labour Force`,
                 `Participation Rate` = `Labour Force`/Population,
                 `Employment Rate` = Employment/Population,
                 `Average Hourly Wage`)]

cut14 <- dt[, .(age = "15+",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant, prov, gender)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                     `Labour Force`, Employment, Unemployment, 
                     `Unemployment Rate` = Unemployment/`Labour Force`,
                     `Participation Rate` = `Labour Force`/Population,
                     `Employment Rate` = Employment/Population,
                  `Average Hourly Wage`)]

cut15 <- dt[, .(gender = "All genders",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant, prov, age)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                        `Labour Force`, Employment, Unemployment, 
                        `Unemployment Rate` = Unemployment/`Labour Force`,
                        `Participation Rate` = `Labour Force`/Population,
                        `Employment Rate` = Employment/Population,
                  `Average Hourly Wage`)]

cut16 <- dt[, .(prov = "Canada",
                Population = sum(FINALWT/12),
                `Labour Force` = sum(FINALWT/12 * (LFSSTAT !=4)),
                Employment = sum(FINALWT/12 * (LFSSTAT %in% c(1,2))),
                Unemployment  = sum(FINALWT/12 * (LFSSTAT == 3)),
                `Average Hourly Wage` = mean(hrlyearn, na.rm=TRUE)),
            by = .(SURVYEAR, immigrant, age, gender)
            ][, .(SURVYEAR, immigrant, prov, age, gender, Population,
                  `Labour Force`, Employment, Unemployment, 
                  `Unemployment Rate` = Unemployment/`Labour Force`,
                  `Participation Rate` = `Labour Force`/Population,
                  `Employment Rate` = Employment/Population,
                  `Average Hourly Wage`)]
  
# append all data sets into one
table_names <- paste0("cut", 1:16)
dt_list <- mget(table_names)

all_indicators <- rbindlist(dt_list)

# move to long format (all indicators should be a column called "indicator")
long_all_indicators <- melt(all_indicators,
                            id.vars = c("SURVYEAR", "immigrant","prov","age",
                                        "gender"),
                            variable.name = "Indicator",
                            value.name = "Value"
                            )

# calculate YOY changes (pct change for levels; pct points for rates)
setorder(long_all_indicators,
         immigrant, prov, age, gender, Indicator, SURVYEAR)

long_all_indicators[grepl("rate", Indicator, ignore.case=TRUE),
                    yoy := c(NA, diff(Value)),
                    by= .(immigrant, prov, age, gender, Indicator)]

long_all_indicators[!grepl("rate", Indicator, ignore.case=TRUE),
                    yoy := Value / shift(Value, type="lag") - 1,
                    by= .(immigrant, prov, age, gender, Indicator)]

# move to wide on immigrant
indicators <- dcast(long_all_indicators, 
      SURVYEAR + prov + age + gender + Indicator ~ immigrant, 
      value.var = "Value")

# format the columns for pretty display
cols_to_format <- c("Permanent residents", "Non-immigrants", "Total population")

indicators[, (cols_to_format) := lapply(.SD, function(x) {
  ifelse(grepl("rate", Indicator, ignore.case = T), 
         paste0(round(x*100,1), "%"),
         ifelse(grepl("wage", Indicator, ignore.case = T), 
                paste0("$",round(x,2)), format(round(x), big.mark=",")))}),
  .SDcols = cols_to_format]

####### Appending the YOY changes
yoy_wide <- dcast(long_all_indicators,
                  SURVYEAR + Indicator + prov + age + gender ~ immigrant,
                  value.var = "yoy")

yoy_wide <- yoy_wide[, (cols_to_format) := lapply(.SD, function(x) x*100),
                     .SDcols = cols_to_format]

# Format the values appropriately and add arrow indicators
yoy_wide[, (cols_to_format) := lapply(.SD, function(x) {
        ifelse(grepl("rate", Indicator, ignore.case = T) & x > 0, 
               paste0("(",sprintf("%.1f", x),") <span style='color:green;'>&#129093;</span>"),
               ifelse(grepl("rate", Indicator, ignore.case = T) & x < 0,
                      paste0("(",sprintf("%.1f", x),") <span style='color:red;'>&#129095;</span>"),
                      ifelse(grepl("rate", Indicator, ignore.case = T) & x == 0,
                             paste0("(",sprintf("%.1f", x),")"),
                             ifelse(x > 0,
                                    paste0("(",sprintf("%.1f%%", x),") <span style='color:green;'>&#129093;</span>"),
                                    ifelse(x < 0,
                                           paste0("(",sprintf("%.1f%%", x),") <span style='color:red;'>&#129095;</span>"),
                                           paste0("(",sprintf("%.1f%%", x),")"))))))}),
        .SDcols = cols_to_format]

# Add the YOY changes
# set the order for both datasets to ensure they match
setorder(indicators, prov, age, gender, Indicator, SURVYEAR)
setorder(yoy_wide, prov, age, gender, Indicator, SURVYEAR)

indicators <- indicators[yoy_wide, 
                         on=.(prov, age, gender, Indicator, SURVYEAR),
                         nomatch=0]

# Use the ':=' operator to modify 'Non-immigrants' in place
indicators[, c("Non-immigrants", "Permanent residents", "Total population") :=
                   .(ifelse(SURVYEAR > 2013, paste(`Non-immigrants`,
                                                    `i.Non-immigrants`),
                            `Non-immigrants`),
                     ifelse(SURVYEAR > 2013, paste(`Permanent residents`,
                                                    `i.Permanent residents`),
                            `Permanent residents`),
                     ifelse(SURVYEAR > 2013, paste(`Total population`,
                                                    `i.Total population`),
                            `Total population`))]

# change ordering for menu options in Shiny
indicators[, `:=` (prov = factor(prov, levels = c("Canada", 
                                                 "Newfoundland and Labrador",
                                                 "Prince Edward Island",
                                                 "Nova Scotia",
                                                 "New Brunswick",
                                                 "Quebec",
                                                 "Ontario",
                                                 "Manitoba",
                                                 "Saskatchewan",
                                                 "Alberta",
                                                 "British Columbia")),
                  age = factor(age, levels = c("15+", "15-24","25-54","55+")))]

# remove extraneous columns (FINAL TABLE FOR SHINY APPLICATION)
indicators <- indicators[,.(SURVYEAR, prov, age, gender, Indicator, 
                            `Non-immigrants`, `Permanent residents`,
                            `Total population`)]

#### Now create an R Shiny DATA Table App --------------------------------------
library(DT)
library(shiny)
library(plotly)

# Create UI
ui <- shinyUI(
        fluidPage(
                
                # Include custom CSS to ensure it's loaded last and overrides 
                # Bootstrap styles
                tags$head(
                        tags$style(HTML("
        .navbar-default {
          background-color: #00abbe !important;
          border-color: #00abbe !important;
        }
        .navbar-default .navbar-nav > li > a,
        .navbar-default .navbar-brand {
          color: #000000 !important;
        font-weight: bold !important;
        }
      "))
                ),
      
      navbarPage(
              
              title = "DDRA",
              
              # Home tab
              tabPanel(
                      "Home",
                      
                      includeHTML("home.Rhtml"),  # Including your HTML content
                      
                      HTML("<br>"), HTML("<br>"),
                      
                      fluidRow(
                              
                              column(3,
                                     selectInput("year", "Year:",
                                                 choices = unique(indicators$SURVYEAR),
                                                 selected = max(indicators$SURVYEAR))),
                              
                              column(3, 
                                     selectInput("geo", "Geography:", 
                                                 choices = levels(indicators$prov),
                                                 selected = "Canada")),
                              
                              column(3,
                                     selectInput("ageGroup", "Age Group:",
                                                 choices = unique(indicators$age),
                                                 selected = "15+")),
                              
                              column(3,
                                     selectInput("gender", "Gender:", 
                                                 choices = unique(indicators$gender),
                                                 selected = "All genders"))
                      ), # end row 1
                      
                      fluidRow(
                              
                              column(12,
                                     DTOutput("dataTable"))
                              
                      ), # end second row
                      
                      fluidRow(
                              
                              column(12,
                                     plotlyOutput("lfs_composition"))
                              
                      ) # end third row
                      
              ), # end Home Tab
              
              # Additional tabs
              tabPanel("Employment"),
              tabPanel("Unemployment"),
              tabPanel("Wages")
              
      ) # end navbar page
        ) # close fluidpage
) # close UI

# Build the server
server <- function(input, output, session) {
  
  # Home Tab: Create the data table
        output$dataTable <- renderDT({

                # filter the data table based on the inputs
                filteredDT <- indicators[SURVYEAR == input$year &
                                                 prov == input$geo &
                                                 age == input$ageGroup &
                                                 gender == input$gender,
                                         .(Indicator, `Permanent residents`, 
                                           `Non-immigrants`, 
                                           `Total population`)
                                         ]
                
                # Define tooltips for column names
                pr_tooltip <- 'Refers to a person who is or has ever been a landed immigrant in Canada.'
                ni_tooltip <- 'Refers to a person who is a Canadian citizen by birth, whether born in or outside of Canada, or a person who has never been a permanent resident or landed immigrant in Canada.'
                
                # Define tooltips for row values in the Indicator column
                indicator_tooltips <- list(
                        "Population" = "Number of persons of working age, 15 years and over.",
                        "Labour Force" = "Number of civilian, non-institutionalized persons 15 years of age and over who, during the reference week, were employed or unemployed.",
                        "Employment" = "Number of persons who, during the reference week, worked for pay or profit, or performed unpaid family work or had a job but were not at work due to own illness or disability, personal or family responsibilities, labour dispute, vacation, or other reason. Those persons on layoff and persons without work but who had a job to start at a definite date in the future are not considered employed.",
                        "Unemployment" = "Number of persons who, during the reference week, were without work, had looked for work in the past four weeks, and were available for work. Those persons on layoff or who had a new job to start in four weeks or less are considered unemployed.",
                        "Unemployment Rate" = "The unemployment rate is the number of unemployed persons expressed as a percentage of the labour force. The unemployment rate for a particular group (age, sex, marital status, etc.) is the number unemployed in that group expressed as a percentage of the labour force for that group.",
                        "Participation Rate" = "The participation rate is the number of labour force participants expressed as a percentage of the population 15 years of age and over. The participation rate for a particular group (age, sex, marital status, etc.) is the number of labour force participants in that group expressed as a percentage of the population for that group.",
                        "Employment Rate" = "The employment rate is the number of persons employed expressed as a percentage of the population 15 years of age and over. The employment rate for a particular group (age, sex, marital status, etc.) is the number employed in that group expressed as a percentage of the population for that group.",
                        "Average Hourly Wage" = "Respondents are asked to report their wage/salary before taxes and other deductions, and include tips, commissions and bonuses. The average wage per hour for employees is calculated from their responses."
                )
                
                # Wrap values in the Indicator column with HTML if a tooltip is defined
                filteredDT[, Indicator := sapply(Indicator, function(ind) {
                        if (!is.null(indicator_tooltips[[ind]])) {
                                htmltools::HTML(sprintf('<span title="%s">%s</span>', indicator_tooltips[[ind]], ind))
                        } else {
                                ind
                        }
                })]
                
                # Add tooltip to the headers
                names(filteredDT) <- c('Indicator', 
                                       sprintf('<span title="%s">%s</span>',
                                               pr_tooltip, 'Permanent residents'),
                                       sprintf('<span title="%s">%s</span>',
                                               ni_tooltip, 'Non-immigrants'),
                                       'Total population')
                
                # Render datatable
                datatable(filteredDT, escape = FALSE, rownames = FALSE,
                          options = list(searching = FALSE,
                                         autowidth = TRUE),
                          caption = htmltools::tags$caption(
                                  style = "caption-side: bottom; text-align: left; color: black; font-style: italic;",
                                  "Note: Values in parentheses represent the year-over year change. For counts, this is the percent change; for rates, this is the percentage point change.",
                                  htmltools::tags$br(),
                                  "Source: Statistics Canada LFS Public Use Micro Files (PUMF), 2013-2023.")
                          )
                
        }) # end render data table function
  
        # Home Tab: create reactive expression based on the inputs for the table
        donut_data_filtered <- reactive({
                
                rbind(long_all_indicators[SURVYEAR == input$year & 
                                            prov == input$geo & 
                                            Indicator == "Labour Force" &
                                                  age == "15+" &
                                                  gender != "All genders",
                                          .(immigrant, plot_type = "gender",
                                            filter = gender, Value)],
                      long_all_indicators[SURVYEAR == input$year &
                                                  Indicator == "Labour Force" &
                                                  prov == input$geo & 
                                                  age != "15+" &
                                                  gender == "All genders",
                                          .(immigrant, plot_type = "age",
                                            filter = age, Value)]
                )
        })
        
        # Home Tab: Create donut plots
        output$lfs_composition <- renderPlotly({
                
                donut_data <- donut_data_filtered()
                
                plot_ly() %>%
                        
                        # Create first donut plot: PR by gender
                        add_pie(data=donut_data[immigrant == "Permanent residents" &
                                                        plot_type == "gender"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=0, column = 0)
                        ) %>%
                        
                        # Create second donut plot: PR by age
                        add_pie(data=donut_data[immigrant == "Permanent residents" &
                                                           plot_type == "age"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=0, column = 1)
                        ) %>%
                        
                        # Create third donut plot: Non-immigrants by gender
                        add_pie(data=donut_data[immigrant == "Non-immigrants" &
                                                        plot_type=="gender"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=1, column = 0)
                        ) %>%
                        
                        # Create fourth donut plot: Non-immigrants by age
                        add_pie(data=donut_data[immigrant == "Non-immigrants" &
                                                        plot_type=="age"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=1, column = 1)
                        ) %>%
                        
                        # Create fifth donut plot: Total population by gender
                        add_pie(data=donut_data[immigrant == "Total population" &
                                                        plot_type=="gender"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=2, column = 0)
                        ) %>%
                        
                        # create sixth donut plot: Total population by age
                        add_pie(data=donut_data[immigrant == "Total population" &
                                                        plot_type=="age"],
                                labels = ~filter,
                                values = ~Value,
                                hole = 0.6,
                                hoverinfo = "label+value+percent",
                                domain = list(row=2, column = 1)
                        ) %>%
                        
                        # add layout features
                        layout(title = list(text = "Labour Force Composition",
                                            x = 0.5, y = 0.98),
                               showlegend = FALSE,
                               grid = list(rows = 3, columns = 2),
                               annotations = list(
                                       # Row titles
                                       list(text = "Permanent Residents", 
                                            x = 0, y = 0.85,
                                            xref = "paper", yref = "paper", 
                                            showarrow = FALSE, font = list(size = 16)),
                                       list(text = "Non-immigrants", 
                                            x = 0, y = 0.5,
                                            xref = "paper", yref = "paper", 
                                            showarrow = FALSE, font = list(size = 16)),
                                       list(text = "Total Population",
                                            x = 0, y = 0.15, 
                                            xref = "paper", yref = "paper", 
                                            showarrow = FALSE, font = list(size = 16)),
                                       
                                       # Column titles
                                       list(text = "Gender",
                                            x = 0.15, y = 1.1,
                                            xref = "paper", yref = "paper", 
                                            showarrow = FALSE, font = list(size = 20)),
                                       list(text = "Age",
                                            x = 0.85, y = 1.1,
                                            xref = "paper", yref = "paper", 
                                            showarrow = FALSE, font = list(size = 20))
                               ),
                               xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
                               yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE)
                        )
                
        }) # close donut plots
        
} # close server function

# Run shiny app
shinyApp(ui=ui, server=server)
             